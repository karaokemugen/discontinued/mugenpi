# How to contribute

Every good will is welcome.

To see how you can help, check :

- [Create an issue or try to resolve one!](https://lab.shelter.moe/karaokemugen/MugenPi/issues)
- [Come to our Discord!](https://discord.gg/a8dMYek)
