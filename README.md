# MugenPi

## Install Karaoke Mugen image on your Raspberry Pi

[Download the latest pre-build image*](https://mugenpi.crystalyx.net/) or build it yourself with the instructions bellow.
To flash the image, [follow theses instructions](https://www.raspberrypi.org/documentation/installation/installing-images/).
You don't have to download the official Raspbian image, flash your custom image instead.

Karaoke Mugen will be launched automatically on boot with a custom light desktop environment.
The first boot is very long, up to 5 minutes (with a black screen during some time). This is normal, the next boot will be much faster.

_* Please note that the pre-build image can be inaccessible while a build is running (displaying a 404 error page). Come back a little later and it will be there._

## Default logins and passwords

The default `admin` password is `karaokemugen`.
**Please change the password as soon as you have done the initial setup !**

The default SSH login and password are the same than the Raspbian distribution (`pi` / `raspberry`).
To view the logs, connect to the Raspberry Pi on SSH and run : `journalctl -u karaokemugen`.

A wifi hotspot is also created with the `KaraokeMugen` SSID and `karaokemugen` password.
Any client connected on this hotspot can go directly on http://mugen.pi to access to Karaoke Mugen.
Moreover, you can connect to the SSH with this domain name (`ssh pi@mugen.pi`).

## Build the image (for advanced users only)

### Requirements

- At least 20 Go of disk drive space.
- A compatible Unix host (could be any Debian or any non-Windows system that can run Docker).
- Up to several hours of your time depending of the capacities of you CPU.

#### On a Debian system

Clone this repository and run the `./build-on-linux.sh` script as root.

/!\ **Your system will be use to build the final image and will be heavily altered.**
Please run on a clean environment without sensitive data on the disk and at your own risks.

#### Via Docker

Clone this repository and run the `./build-on-docker.sh` script.

#### Install directly on your Raspberry Pi

/!\ **Your system will be heavily altered, this method is highly NOT recommended and will take more than 5 hours !**
Please run on a clean environment without sensitive data on the disk and at your own risks.

Clone this repository and run the `./build-on-rpi.sh` script as root.

## Update Karaoke Mugen on MugenPi

As pi user, go to the `/opt/karaokemugen` folder and update Karaoke Mugen according to the [README](https://lab.shelter.moe/karaokemugen/karaokemugen-app/blob/master/README.md).
You may have to do some additional tasks regarding the Karaoke Mugen app [CHANGELOG](https://lab.shelter.moe/karaokemugen/karaokemugen-app/blob/master/CHANGELOG.md).

## Known issues

- Can't be build on Windows because of permissions issues with SMB (HyperV / VirtualBox folder sharing system).
