#!/bin/bash -e

if [[ "$(id -u)" != "0" ]]; then
	echo "Please run as root" 1>&2
	exit 1
fi

apt-get update
apt-get install -y $(cat ./files/04-install-karaoke-mugen/00-packages)
./files/04-install-karaoke-mugen/01-run-chroot.sh
./files/04-install-karaoke-mugen/02-run.sh
